function getEle(id){
    return document.getElementById(id);
}
function caculateTip(){
    //get input value when users fill the boxes
    var totalBill = getEle("billamt").value;
    console.log(totalBill);
    var service = getEle("serviceQual").value;
    var numberOfPeopleShared = getEle("peopleamt").value; 

    //validation
    if (totalBill ==="" || service==0){
        alert("Please choose the value");
        return;
    }
    // check whether is there anybody enter the number of people sharing TIPS
if(numberOfPeopleShared ==="" || numberOfPeopleShared <= 1){
    numberOfPeopleShared = 1;
    getEle("each").style.display ="none";
}
    else{
        getEle("each").style.display = "block";
    }
//Calculate TIP
var totalAmountOfTip = (totalBill * service)/numberOfPeopleShared;
// round to a two-digit decimal
 totalAmountOfTip= Math.round(totalAmountOfTip *100)/100;
// make sure there are 2 digits in the decimal part
totalAmountOfTip=totalAmountOfTip.toFixed(2);

getEle("totalTip").style.display="block";
getEle("tip").innerHTML= totalAmountOfTip;

}
//hide
getEle("totalTip").style.display="none";
getEle("each").style.display="none";
// assign event into ONCLICK to check when you enter the value (total bill)
getEle("calculate").onclick= function (){
    caculateTip();
    //button:calculate
}


